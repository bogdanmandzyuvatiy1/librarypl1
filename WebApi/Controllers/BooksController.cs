using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private IBookService BookService { get; }

        public BooksController(IBookService bookService)
        {
            BookService = bookService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<BookModel>> GetByFilter([FromQuery] FilterSearchModel model)
        {
            var result = BookService.GetByFilter(model);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<BookModel>>> GetById(int id)
        {
            var result = await BookService.GetByIdAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] BookModel bookModel)
        {
            await BookService.AddAsync(bookModel);
            return Ok(BookService.GetAll().Last());
        }

        [HttpPut]
        public async Task<ActionResult> Update(BookModel bookModel)
        {
            await BookService.UpdateAsync(bookModel);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await BookService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}