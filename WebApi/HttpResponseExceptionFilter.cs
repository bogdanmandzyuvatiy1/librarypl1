﻿using Business.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApi
{
    public class HttpResponseExceptionFilter : IActionFilter
    {

        public void OnActionExecuting(ActionExecutingContext context)
        {
            // Executes before action result
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is LibraryException exception)
            {
                context.Result = new ObjectResult(exception.Message)
                {
                    StatusCode = exception.Status,
                };
                context.ExceptionHandled = true;
            }
        }
    }
}