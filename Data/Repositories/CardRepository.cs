﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class CardRepository : Repository<Card>, ICardRepository
    {
        
        public CardRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
        }

        public IQueryable<Card> FindAllWithDetails()
        {
            return FindAll().Include(c=>c.Books).Include(c=>c.Reader);
        }

        public async Task<Card> GetByIdWithDetailsAsync(int id)
        {
            return await FindAll().Include(x => x.Books).Include(x => x.Reader).SingleOrDefaultAsync(x => x.Id == id);
        }

    }
}
