using System.Linq;
using AutoMapper;
using Business.Models;
using Data.Entities;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Book, BookModel>()
                .ForMember(p => p.CardsIds, o => o.MapFrom(x => x.Cards.Select(y => y.CardId)))
                .ReverseMap();

            CreateMap<Card, CardModel>()
                .ForMember(d => d.BooksIds, o => o.MapFrom(s => s.Books.Select(b=>b.BookId))).ReverseMap();

            CreateMap<Reader, ReaderModel>()
                .ForMember(x => x.Phone, o => o.MapFrom(x => x.ReaderProfile.Phone))
                .ForMember(d => d.Address, opt => opt.MapFrom(s => s.ReaderProfile.Address))
                .ForMember(d => d.CardsIds, opt => opt.MapFrom(s => s.Cards.Select(c => c.Id)))
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.ReaderProfile.ReaderId))
                .ReverseMap();

        }
    }
}