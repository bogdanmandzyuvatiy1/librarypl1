﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Book: BaseEntity
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }

        public List<History> Cards { get; set; }
    }
}
