﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;

namespace Business.Comparers
{
    class ReaderActivityComparer : EqualityComparer<ReaderActivityModel>
    {
        public override bool Equals(ReaderActivityModel x, ReaderActivityModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.ReaderId == y.ReaderId &&
                   x.BooksCount == y.BooksCount &&
                   string.Equals(x.ReaderName, y.ReaderName);
        }

        public override int GetHashCode(ReaderActivityModel obj)
        {
            int hCode = obj.BooksCount ^ obj.ReaderId;
            return hCode.GetHashCode();
        }
    }
}
