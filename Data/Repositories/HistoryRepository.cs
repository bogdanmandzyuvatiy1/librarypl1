﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class HistoryRepository : Repository<History>, IHistoryRepository
    {
        public IQueryable<History> GetAllWithDetails()
        {
            return FindAll().Include(h => h.Book).Include(h=>h.Card).ThenInclude(c => c.Reader);
        }
        public HistoryRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
        }
    }
}
