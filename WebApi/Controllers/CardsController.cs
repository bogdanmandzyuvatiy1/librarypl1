﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CardsController : Controller
    {
        private ICardService CardService { get; }

        public CardsController(ICardService cardService)
        {
            CardService = cardService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CardModel>> Get()
        {
            var result = CardService.GetAll();

            if (result == null)
            {
                return NotFound();
            }
            
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CardModel>>> GetById(int id)
        {
            var result = await CardService.GetByIdAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


        [HttpPost]
        public async Task<ActionResult> Add([FromBody] CardModel cardModel)
        {
            await CardService.AddAsync(cardModel);
            return Ok(CardService.GetAll().Last());
        }

        [HttpPut]
        public async Task<ActionResult> Update(CardModel cardModel)
        {
            await CardService.UpdateAsync(cardModel);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await CardService.DeleteByIdAsync(id);
            return Ok();
        }


        [HttpGet("{id}/books")]
        public ActionResult<IEnumerable<BookModel>> GetAllCardBooks(int id)
        {
            var result = CardService.GetBooksByCardId(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


        [HttpPost("{cardId}/books/{bookId}")]
        public async Task<ActionResult> TakeBook(int cardId, int bookId)
        {
            await CardService.TakeBookAsync(cardId, bookId);
            return Ok();
        }

        [HttpDelete("{cardId}/books/{bookId}")]
        public async Task<ActionResult> HandOverBook(int cardId, int bookId)
        {
            await CardService.HandOverBookAsync(cardId, bookId);
            return Ok();
        }
    }
}
