﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticController : Controller
    {
        private  IStatisticService StatisticService { get; }

        public StatisticController(IStatisticService statisticService)
        {
            StatisticService = statisticService;
        }

        [HttpGet("popularBooks")]
        public ActionResult<IEnumerable<BookModel>> GetMostPopularBooks(int bookCount)
        {
            var result = StatisticService.GetMostPopularBooks(bookCount);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


        [HttpGet("biggestReaders")]
        public ActionResult<IEnumerable<ReaderActivityModel>> GetReadersWhoTookTheMostBooks(int readersCount, DateTime firstDate, DateTime lastDate)
        {
            var result = StatisticService.GetReadersWhoTookTheMostBooks(readersCount, firstDate, lastDate);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
