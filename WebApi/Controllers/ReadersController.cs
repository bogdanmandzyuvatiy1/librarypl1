﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Data.Interfaces;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReadersController : Controller
    {
        private IReaderService ReaderService{ get; }

        public ReadersController(IReaderService readerService)
        {
            ReaderService = readerService;
        }

        public async Task<ActionResult> Add([FromBody] ReaderModel readerModel)
        {
            await ReaderService.AddAsync(readerModel);
            var reader = ReaderService.GetAll().Last();
            return CreatedAtAction(nameof(GetById), new { Id = reader.Id }, readerModel);
        }

        [HttpGet]
        public ActionResult<IEnumerable<ReaderModel>> Get()
        {
            var result =  ReaderService.GetAll();

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ReaderModel>>> GetById(int id)
        {
            var result = await ReaderService.GetByIdAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("dontreturnbooks")]
        public ActionResult<IEnumerable<ReaderModel>> GetReadersThatDontReturnBooks()
        {
            var readers = ReaderService.GetReadersThatDontReturnBooks();
            if (readers == null)
            {
                return NotFound();
            }

            return Ok(readers);
        }

        [HttpPut]
        public async Task<ActionResult> Update(ReaderModel readerModel)
        {
            await ReaderService.UpdateAsync(readerModel);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await ReaderService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
