﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        public BookRepository(LibraryDbContext libraryDbContext): base(libraryDbContext)
        {
        }

        public IQueryable<Book> FindAllWithDetails()
        {
            return FindAll().Include(b => b.Cards);
        }

        public async Task<Book> GetByIdWithDetailsAsync(int id)
        {
            return await FindAll().Include(x => x.Cards).SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}
