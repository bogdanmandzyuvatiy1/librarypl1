using Data.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using Data.Repositories;

namespace Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public UnitOfWork(LibraryDbContext libraryDbContext)
        {
            context = libraryDbContext;
        }

        private readonly LibraryDbContext context;

        private IBookRepository bookRepository;
        private ICardRepository cardRepository;
        private IHistoryRepository historyRepository;
        private IReaderRepository readerRepository;
        
       

        public IBookRepository BookRepository
        {
            get
            {
                if (this.bookRepository == null)
                {
                    this.bookRepository = new BookRepository(context);
                }

                return bookRepository;
            }
        }

        public ICardRepository CardRepository
        {
            get
            {
                if (this.cardRepository == null)
                {
                    this.cardRepository = new CardRepository(context);
                }

                return cardRepository;
            }
        }

        public IHistoryRepository HistoryRepository
        {
            get
            {
                if (this.historyRepository == null)
                {
                    this.historyRepository = new HistoryRepository(context);
                }

                return historyRepository;
            }
        }

        public IReaderRepository ReaderRepository
        {
            get
            {
                this.readerRepository ??= new ReaderRepository(context);

                return readerRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
            {
                
                context.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}