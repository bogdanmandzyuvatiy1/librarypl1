﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Business.Services
{
    public class ReaderService : IReaderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReaderService(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(ReaderModel model)
        {
            if (model is null)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }

            var check = model.GetType().GetProperties()
                .Where(pi => pi.PropertyType == typeof(string))
                .Select(pi => (string)pi.GetValue(model))
                .Any(string.IsNullOrEmpty);

            if (check)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }
            

            await _unitOfWork.ReaderRepository.AddAsync(_mapper.Map<Reader>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.ReaderRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<ReaderModel> GetAll()
        {
            var readers = _unitOfWork.ReaderRepository
                .GetAllWithDetails()
                .AsNoTracking()
                .ToList();

            return _mapper.Map<IEnumerable<ReaderModel>>(readers);
        }

        public async Task<ReaderModel> GetByIdAsync(int id)
        {
            var source = await _unitOfWork.ReaderRepository
                .GetByIdWithDetails(id);

            return _mapper.Map<ReaderModel>(source);
        }

        public IEnumerable<ReaderModel> GetReadersThatDontReturnBooks()
        {
            var cardIds = _unitOfWork.HistoryRepository.FindAll().Where(h => h.ReturnDate == default).Select(h => h.CardId).ToList();
            var readerIds = _unitOfWork.CardRepository.FindAll().Where(c => cardIds.Contains(c.Id))
                .Select(c => c.ReaderId);
            var readers = _unitOfWork.ReaderRepository.GetAllWithDetails().Where(r => readerIds.Contains(r.Id));
            
            return _mapper.Map<IEnumerable<ReaderModel>>(readers);
        }

        public async Task UpdateAsync(ReaderModel model)
        {
            if (model is null)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }

            var check = model.GetType().GetProperties()
                .Where(pi => pi.PropertyType == typeof(string))
                .Select(pi => (string)pi.GetValue(model))
                .Any(string.IsNullOrEmpty);

            if (check)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }

            var x = _mapper.Map<Reader>(model);
            x.ReaderProfile.Reader = x;
            _unitOfWork.ReaderRepository.Update(x);
            await _unitOfWork.SaveAsync();
        }
    }
}
