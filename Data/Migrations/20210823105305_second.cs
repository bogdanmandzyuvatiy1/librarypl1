﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Card_Reader_ReaderId",
                table: "Card");

            migrationBuilder.DropForeignKey(
                name: "FK_History_Book_BookId",
                table: "History");

            migrationBuilder.DropForeignKey(
                name: "FK_History_Card_CardId",
                table: "History");

            migrationBuilder.DropForeignKey(
                name: "FK_ReaderProfiles_Reader_ReaderId",
                table: "ReaderProfiles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Reader",
                table: "Reader");

            migrationBuilder.DropPrimaryKey(
                name: "PK_History",
                table: "History");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Card",
                table: "Card");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Book",
                table: "Book");

            migrationBuilder.RenameTable(
                name: "Reader",
                newName: "Readers");

            migrationBuilder.RenameTable(
                name: "History",
                newName: "Histories");

            migrationBuilder.RenameTable(
                name: "Card",
                newName: "Cards");

            migrationBuilder.RenameTable(
                name: "Book",
                newName: "Books");

            migrationBuilder.RenameIndex(
                name: "IX_History_CardId",
                table: "Histories",
                newName: "IX_Histories_CardId");

            migrationBuilder.RenameIndex(
                name: "IX_History_BookId",
                table: "Histories",
                newName: "IX_Histories_BookId");

            migrationBuilder.RenameIndex(
                name: "IX_Card_ReaderId",
                table: "Cards",
                newName: "IX_Cards_ReaderId");

            migrationBuilder.AlterColumn<int>(
                name: "ReaderId",
                table: "ReaderProfiles",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ReaderProfileId",
                table: "Readers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Readers",
                table: "Readers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Histories",
                table: "Histories",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cards",
                table: "Cards",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Books",
                table: "Books",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Readers_ReaderProfileId",
                table: "Readers",
                column: "ReaderProfileId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Readers_ReaderId",
                table: "Cards",
                column: "ReaderId",
                principalTable: "Readers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Histories_Books_BookId",
                table: "Histories",
                column: "BookId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Histories_Cards_CardId",
                table: "Histories",
                column: "CardId",
                principalTable: "Cards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Readers_ReaderProfiles_ReaderProfileId",
                table: "Readers",
                column: "ReaderProfileId",
                principalTable: "ReaderProfiles",
                principalColumn: "ReaderId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Readers_ReaderId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Histories_Books_BookId",
                table: "Histories");

            migrationBuilder.DropForeignKey(
                name: "FK_Histories_Cards_CardId",
                table: "Histories");

            migrationBuilder.DropForeignKey(
                name: "FK_Readers_ReaderProfiles_ReaderProfileId",
                table: "Readers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Readers",
                table: "Readers");

            migrationBuilder.DropIndex(
                name: "IX_Readers_ReaderProfileId",
                table: "Readers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Histories",
                table: "Histories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cards",
                table: "Cards");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Books",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "ReaderProfileId",
                table: "Readers");

            migrationBuilder.RenameTable(
                name: "Readers",
                newName: "Reader");

            migrationBuilder.RenameTable(
                name: "Histories",
                newName: "History");

            migrationBuilder.RenameTable(
                name: "Cards",
                newName: "Card");

            migrationBuilder.RenameTable(
                name: "Books",
                newName: "Book");

            migrationBuilder.RenameIndex(
                name: "IX_Histories_CardId",
                table: "History",
                newName: "IX_History_CardId");

            migrationBuilder.RenameIndex(
                name: "IX_Histories_BookId",
                table: "History",
                newName: "IX_History_BookId");

            migrationBuilder.RenameIndex(
                name: "IX_Cards_ReaderId",
                table: "Card",
                newName: "IX_Card_ReaderId");

            migrationBuilder.AlterColumn<int>(
                name: "ReaderId",
                table: "ReaderProfiles",
                type: "int",
                nullable: false,
                oldClrType: typeof(int))
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Reader",
                table: "Reader",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_History",
                table: "History",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Card",
                table: "Card",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Book",
                table: "Book",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Card_Reader_ReaderId",
                table: "Card",
                column: "ReaderId",
                principalTable: "Reader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_History_Book_BookId",
                table: "History",
                column: "BookId",
                principalTable: "Book",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_History_Card_CardId",
                table: "History",
                column: "CardId",
                principalTable: "Card",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ReaderProfiles_Reader_ReaderId",
                table: "ReaderProfiles",
                column: "ReaderId",
                principalTable: "Reader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
