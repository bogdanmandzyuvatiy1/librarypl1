﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Business.Validation
{
    [Serializable]
    public class LibraryException: Exception
    {
        public string Property { get; protected set; }
        public int Status { get; set; } = 400;

        public LibraryException(string message, string prop) : base(message)
        {
            Property = prop;
        }

        public LibraryException(string message): base(message)
        {
        }

        protected LibraryException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
