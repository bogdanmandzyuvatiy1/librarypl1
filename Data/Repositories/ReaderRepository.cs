﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ReaderRepository : Repository<Reader>, IReaderRepository
    {
        public ReaderRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
        }

        public IQueryable<Reader> GetAllWithDetails()
        {
            return FindAll().Include(r => r.ReaderProfile).Include(x => x.Cards);
        }

        public async Task<Reader> GetByIdWithDetails(int id)
        {
            return await FindAll().Include(x => x.Cards).Include(x => x.ReaderProfile).SingleOrDefaultAsync(x => x.Id == id);
        }

        public new void Update(Reader entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.Entry(entity.ReaderProfile).State = EntityState.Modified;
        }
    }
}
