﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Business.Comparers
{
    public class BookComparer: EqualityComparer<Book>
    {
        public override bool Equals(Book x, Book y)
        {
            if (x == null && y == null)
                return true;
            else if (x == null || y == null)
                return false;

            return (x.Id == y.Id &&
                    x.Author == y.Author &&
                    x.Year == y.Year &&
                    x.Title == y.Title);
        }

        public override int GetHashCode(Book obj)
        {
            int hCode = obj.Id ^ obj.Year;
            return hCode.GetHashCode();
        }
    }
}
