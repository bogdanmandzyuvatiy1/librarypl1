﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;

namespace Business.Services
{
    public class BookService : IBookService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BookService(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<BookModel> GetAll()
        {
            var source = _unitOfWork.BookRepository
                .FindAllWithDetails()
                .ToList();

            return _mapper.Map<IEnumerable<BookModel>>(source);
        }

        public async Task<BookModel> GetByIdAsync(int id)
        {
            var source = await _unitOfWork.BookRepository
                .GetByIdWithDetailsAsync(id);

            return _mapper.Map<BookModel>(source);
        }

        public async Task AddAsync(BookModel model)
        {
            if (model is null || String.IsNullOrEmpty(model.Title) || string.IsNullOrEmpty(model.Author) || model.Year > DateTime.Now.Year || model.Year < 0)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }

            await _unitOfWork.BookRepository.AddAsync(_mapper.Map<Book>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(BookModel model)
        {
            if (model is null || String.IsNullOrEmpty(model.Title) || string.IsNullOrEmpty(model.Author) || model.Year > DateTime.Now.Year || model.Year < 0)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }

            _unitOfWork.BookRepository.Update(_mapper.Map<Book>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.BookRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<BookModel> GetByFilter(FilterSearchModel filterSearch)
        {
            if (filterSearch is null)
            {
                throw new LibraryException("Argument is wrong", nameof(filterSearch));
            }

            var query = _unitOfWork.BookRepository.FindAllWithDetails();
            IEnumerable<Book> books;

            if (filterSearch.Author == null && filterSearch.Year == 0)
            {
                books = query.ToList();
            }
            else
            {
                books = query
                    .Where(b => b.Author == filterSearch.Author || b.Year == filterSearch.Year).ToList();
            }
            

            return _mapper.Map<IEnumerable<BookModel>>(books);

        }
    }
}
