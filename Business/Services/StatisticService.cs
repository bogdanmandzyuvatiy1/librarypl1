﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Business.Comparers;
using Business.Interfaces;
using Business.Models;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace Business.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public StatisticService(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<BookModel> GetMostPopularBooks(int bookCount)
        {
            var booksIds = _unitOfWork.HistoryRepository.GetAllWithDetails()
                .GroupBy(h => h.BookId)
                .OrderByDescending(g => g.Count())
                 .Select(g => g.Key)
                .Take(bookCount).ToList();

            
            var books = _unitOfWork.HistoryRepository.GetAllWithDetails()
                .Where(h => booksIds
                    .Contains(h.BookId))
                .Select(b => b.Book).AsEnumerable().Distinct(new BookComparer());

            return _mapper.Map<IEnumerable<BookModel>>(books);
        }

        public IEnumerable<ReaderActivityModel> GetReadersWhoTookTheMostBooks(int readersCount, DateTime firstDate, DateTime lastDate)
        {
            return _unitOfWork.HistoryRepository.GetAllWithDetails()
                .Where(h => h.TakeDate >= firstDate && h.ReturnDate < lastDate && h.ReturnDate != default)
                .Select(h => new ReaderActivityModel()
                {
                    ReaderId = h.Card.Reader.Id,
                    ReaderName = h.Card.Reader.Name,
                    BooksCount = _unitOfWork.HistoryRepository.GetAllWithDetails()
                        .Where(h1 => h1.TakeDate > firstDate && h1.ReturnDate < lastDate && h.ReturnDate != default)
                        .Count(h1 => h1.Card.ReaderId == h.Card.ReaderId)
                })
                .AsEnumerable().Distinct(new ReaderActivityComparer())
                .OrderByDescending(r => r.BooksCount)
                .Take(1);
        }
    }
}
