using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;

namespace Business.Services
{
    public class CardService : ICardService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CardService(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CardModel model)
        {
            if (model is null || model.Created > DateTime.Now)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }

            await _unitOfWork.CardRepository.AddAsync(_mapper.Map<Card>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.CardRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<CardModel> GetAll()
        {
            var source = _unitOfWork.CardRepository
                .FindAllWithDetails()
                .ToList();

            return _mapper.Map<IEnumerable<CardModel>>(source);
        }

        public async Task UpdateAsync(CardModel model)
        {
            if (model is null || model.Created > DateTime.Now)
            {
                throw new LibraryException("Argument is wrong", nameof(model));
            }

            _unitOfWork.CardRepository.Update(_mapper.Map<Card>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task<CardModel> GetByIdAsync(int id)
        {
            var source = await _unitOfWork.CardRepository
                .GetByIdWithDetailsAsync(id);

            return _mapper.Map<CardModel>(source);
        }

        public IEnumerable<BookModel> GetBooksByCardId(int cardId)
        {
            var books = _unitOfWork.BookRepository.FindAllWithDetails().Where(b => b.Cards.Any(c => c.CardId == cardId))
                .AsEnumerable();
            return _mapper.Map<IEnumerable<BookModel>>(books);
        }

        public async Task HandOverBookAsync(int cartId, int bookId)
        {
            var history = _unitOfWork.HistoryRepository.FindAll().Where(h => h.BookId == bookId && h.CardId == cartId)
                .FirstOrDefault(h => h.TakeDate != default && h.ReturnDate == default);

            if (history is null)
            {
                throw new LibraryException("Book hasn't been taken by this user");
            }

            history.ReturnDate = DateTime.Now;
            _unitOfWork.HistoryRepository.Update(history);
            await _unitOfWork.SaveAsync();
        }

        public async Task TakeBookAsync(int cartId, int bookId)
        {

            var book = await _unitOfWork.BookRepository.GetByIdAsync(bookId);
            var card = await _unitOfWork.CardRepository.GetByIdAsync(cartId);

            if (book is null || card is null)
            {
                throw new LibraryException("Not existing book or card");
            }

            var result = _unitOfWork.HistoryRepository.FindAll().Where(h =>
                h.BookId == bookId).All(h => h.ReturnDate != default && h.ReturnDate < DateTime.Now);

            if (!result)
            {
                throw new LibraryException("This has have already been taken and hasn't been returned yet");
            }

            var history = new History()
            {
                Book = book,
                Card = card,
                BookId = bookId,
                CardId = cartId,
                TakeDate = DateTime.Now
            };

            book.Cards.Add(history);
            card.Books.Add(history);
            _unitOfWork.CardRepository.Update(card);
            _unitOfWork.BookRepository.Update(book);
            await _unitOfWork.HistoryRepository.AddAsync(history);
            await _unitOfWork.SaveAsync();
        }

    }
}
